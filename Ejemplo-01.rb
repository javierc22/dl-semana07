# -------------- Uso básico de HASH --------------- #
# Ejemplo con Arrays
products = ['product1', 'product2', 'product3', 'product4']
prices = [100, 500, 50, 250]

# Obtener el índice:
search = 'product1'
puts products.index(search)
puts products.index('product4')

# Obtener el precio:
search = 'product1'
puts prices[products.index(search)]
search = 'product2'
puts prices[products.index(search)]
search = 'product3'
puts prices[products.index(search)]

# Ejemplo con HASH
products_2 = {'product1' => 100, 'product2' => 500, 'product3' => 1000}

search = 'product1'
puts products_2[search]
search = 'product2'
puts products_2[search]
search = 'product3'
puts products_2[search]
# ------------- CLAVES COMO STRING O SIMBOLO ------------ #
# Clave como hash
colors = {'red' => 'ff0000', 'green' => '00ff00', 'blue' => '0000ff'}
puts colors['red']

# Clave como símbolo
colors_2 = { :red => 'ff0000', :green => '00ff00', :blue => '0000ff'}
puts colors_2[:red]
# ----------- OTRA NOTACION PARA HASH ----------- #

# Los índices en un Hash se llaman Keys (claves)

hash = { a: 5, b: 'Hola', c: 10.12}
puts hash[:a] # imprime 5
puts hash[:b] # imprime Hola
puts hash[:c] # imprime 10.12
# ---------- ITERANDO UN HASH ----------- #
edades = { Oscar: 27, Javier: 31, Francisca: 32, Alejandro: 19 }

edades.each { |key, _value| puts key }
edades.each { |_key, value| puts value }
edades.each { |key, value| puts "#{key} => #{value}"}
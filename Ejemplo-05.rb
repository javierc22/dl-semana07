# ----------- AGREGANDO Y BORRANDO ELEMENTOS ----------- #

# Agregando elementos:
hash = {}
hash[:nuevo] = 'Nuevo valor 1'
hash['nuevo'] = 'Nuevo valor 2'

puts hash

# Eliminando elementos:
notas = { Javier: 8, Julian: 6, Pedro:5}
notas.delete(:Julian)
puts notas

# Un Array se puede convertir en Hash, pero debe tener la siguiente estructura:
array = [[1,2], [3,4], [5,6]]
print array.to_h
puts

# Para convertir un hash en array se usa el método 'hash.to_a'
hash_2 = { 1 => 2, 3 => 4, 5 => 6}
print hash_2.to_a
# ---------- OBJECT_ID ------------- #
# ¿Cómo podemos saber si dos variable referencian al mismo objeto?
# Se puede con .object_id
a = 2
puts a.object_id

a = 3
puts a.object_id

# Si dos variables tienen el mismo object_id es peligroso hacer un cambio
# Caso seguro:
a = 'hola'
b= 'hola'
puts a.object_id == b.object_id # retorna false

# Caso inseguro:
a = 'hola'
b = a
puts a.object_id == b.object_id # retorna true

# Object_id y los Símbolos
# El mismo object_id en los símbolos no es un problema porque son inmutables, osea no lo podemos modificar
# sólo podemos asignar un símbolo nuevo

# Caso seguro:
a = :hola
b = :hola
puts a.object_id == b.object_id # retorna true

# Caso seguro:
a = :hola
b = a
puts a.object_id == b.object_id # retorna true
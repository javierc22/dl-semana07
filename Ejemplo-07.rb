# ------------- MUTABILIDAD EN HASH Y ARRAY ------------ #
# Los arrays son mutables:
a = [9,4,6,2.0,"hola"]
b = a
# => a.object_id == b.object_id # true
puts b
# => [9,4,6,2.0,"hola"]
b[0] = 8
# cambian a y b => [8,4,6,2.0,"hola"]
puts a[0]
# => 8

# Los hash también son mutables
datos = {"usuario":"gonzalo", "password":"secreto"}
otros_datos = datos
otros_datos["usuario"] = "nuevo_password"
# => Agrego nuevo valor a la clave "usuario".
datos["usuario"]
# => nuevo_password 

# SI EN ALGÚN MOMENTO NECESITAMOS HACER UNA COPIA PARA EVITAR PROBLEMAS LO PODEMOS HACER CON CLONE
arr1 = [1, 2, 3]
arr2 = arr1.clone
arr1[4] = 'cambio'
print arr2 # [1, 2, 3]

# MORALEJA
# SI DOS VARIABLES TIENEN EL MISMO OBJECT_ID ES PELIGROSO HACER UN CAMBIO
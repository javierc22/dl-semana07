# ------------ MAP, COLLECT, SELECT Y REJECT ------------- #
# El bloque .each
arr1 = ['Arroz', 'Leche', 'Café']
arr1.each { |item| puts "Debo comprar #{item}"}

# Map y Collect son lo mismo, por lo que sólo necesitamos aprender .map
# .map devuelve un array con el resultado de aplicar la operación específica a cada elemento

# Forma normal:
a = [1,2,3,4,5,6,7]
b = a.map do |e|
  e * 2
end

# Forma express:
a = [1,2,3,4,5,6,7]
b = a.map { |e| e * 2 }

print b # => [2, 4, 6, 8, 10, 12, 14]
puts

# ---------- SELECT Y REJECT --------------- #
# Select:
a = [1,2,3,4,5,6,7]
b = a.select { |e| e.even? } # seleccionamos todos los pares.
print b # => [2,4,6]
puts

# Reject:
a = [1,2,3,4,5,6,7]
b = a.reject { |e| e.even? } # rechazar todos los pares.
print b # => [1, 3, 5, 7]
puts

# .select devuelve un array con todos los elementos que cumplen la condición 
# .reject devuelve un array con los elementos que no la cumplen
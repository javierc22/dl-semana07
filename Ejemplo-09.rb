# ---------- EJERCICIO DE MAP Y SELECT ------------ #
# 1. Iterar el arreglo y mostrar la cantidad de caracteres de cada uno de los nombres
# 2. Utilizar un map para generar un array con la cantidad de caracteres de cada nombre
# 3. Utilizar select para generar un nuevo array con todos los nombres que tengan más de 5 letras

names = ["Violeta", "Andino", "Clemente", "Javiera", "Paula", "Pia", "Ray"]

# 1:
names.each { |nombre| puts nombre.length } # => 7, 6, 8, 7, 5, 3, 3

# 2:
array_1 = names.map { |nombre| nombre.length }
print array_1 # => [7, 6, 8, 7, 5, 3, 3]
puts

# 3:
array_2 = names.select { |nombre| nombre.length > 5 }
print array_2 # => ["Violeta", "Andino", "Clemente", "Javiera"]
puts
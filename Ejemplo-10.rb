# ----------- INJECT ------------- #
a = [1,2,3,4,5,6,7]
b = a.inject(0) { |sum, e| sum + e }
puts b # => 28 
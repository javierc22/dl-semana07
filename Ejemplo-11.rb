# ----------------- GROUP_BY --------------------- #
# Con group_by podemos agrupar por cualquier criterio que queramos
a = []
# Agrugar por tipo de dato:
a.group_by { |element| element.class }

# Agrupar por elemento:
a.group_by { |element| element }

# Agrupar por condición:
a.group_by { |element| element.even? }

# Ejemplo:
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print b.group_by { |e| e.even? } # => {false=>[1, 3, 5, 7, 9], true=>[2, 4, 6, 8, 10]}
puts
print b.group_by { |e| e.odd? } # => {true=>[1, 3, 5, 7, 9], false=>[2, 4, 6, 8, 10]}
puts
print b.group_by { |e| e } # => {1=>[1], 2=>[2], 3=>[3], 4=>[4], 5=>[5], 6=>[6], 7=>[7], 8=>[8], 9=>[9], 10=>[10]}
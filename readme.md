## Desafío Latam - Semana 07

#### Introducción

1. Inicio Semana 7

#### Ruby: Hash

2. Intro a símbolos
3. Intro a Hash
4. Uso básico de Hash
5. Claves como String o Símbolo
6. Otra notación para Hash
7. Iterando un Hash
8. Agregando y borrando elementos
9. Variables y Objetos
10. Object_id
11. Mutabilidad en Hash y Array
12. Map y Collect
13. Ejercicio de Map y Select
14. Inject
15. Group_by
16. Antes del Quiz
17. Quiz
18. Cierre Semana 07

Documentación oficial de Ruby:

* http://ruby-doc.org/

También podrías encontrar respuestas en Api DocK ,(que te será muy útil cuando empieces con Rails ) , o simplemente buscando directo en un buscador (Google)

* https://apidock.com/ruby